window.devetty = (new (class {
    VERSION = 0.00002
    constructor() {
        if (!localStorage.getItem("devetty-version")) {
            localStorage.setItem("devetty-version", this.VERSION)
        } else {
            var version = parseFloat(localStorage.getItem("devetty-version"))
            if (version != this.VERSION) {
                localStorage.removeItem("devetty-version")
                location.reload()
            }
        }
    }
    dragging = class {
        constructor(options = {}) {
            var draggable_items = options.draggable_items ? options.draggable_items : ".draggable"
            var dropzone_items = options.dropzone_items ? options.dropzone_items : ".dropzone"
            var dragging_element = null
            var dragging_element_clone = null
            var dropzone_element = null
            var collapse = function (element_1, element_2) {
                var a = element_1.getBoundingClientRect()
                var b = element_2.getBoundingClientRect()
                return a.left <= b.right && b.left <= a.right && a.top <= b.bottom && b.top <= a.bottom
            }
            document.querySelectorAll(draggable_items).forEach(function (element) {
                element.setAttribute("draggable", "false")
                element.onmousedown = function (event) {
                    dragging_element = element
                    dragging_element_clone = element.cloneNode(true)
                    dragging_element_clone.className = dragging_element_clone.className.replace(/\bdrag-element-source\b/, 'drag-element-item')
                    element.parentElement.appendChild(dragging_element_clone)
                    dragging_element_clone.style.position = "fixed"
                    dragging_element_clone.style.width = element.getBoundingClientRect().width + "px"
                    dragging_element_clone.style.height = element.getBoundingClientRect().height + "px"
                    dragging_element_clone.style.top = element.getBoundingClientRect().top + "px"
                    dragging_element_clone.style.left = element.getBoundingClientRect().left + "px"
                    if (options.ondragstart) options.ondragstart(event, element)
                }
                element.ontouchstart = function (event) {
                    document.documentElement.classList.add("html-fixed")
                    dragging_element = element
                    dragging_element_clone = element.cloneNode(true)
                    dragging_element_clone.className = dragging_element_clone.className.replace(/\bdrag-element-source\b/, 'drag-element-item')
                    element.parentElement.appendChild(dragging_element_clone)
                    dragging_element_clone.style.position = "fixed"
                    dragging_element_clone.style.width = element.getBoundingClientRect().width + "px"
                    dragging_element_clone.style.height = element.getBoundingClientRect().height + "px"
                    dragging_element_clone.style.top = element.getBoundingClientRect().top + "px"
                    dragging_element_clone.style.left = element.getBoundingClientRect().left + "px"
                    if (options.ondragstart) options.ondragstart(event, element)
                }
                window.onmouseup = function (event) {
                    dragging_element = null
                    if (dragging_element_clone) {
                        dragging_element_clone.remove()
                        dragging_element_clone = null
                    }
                    if (dropzone_element) {
                        if (options.ondrop) options.ondrop(event, dropzone_element)
                    } else {
                        if (options.ondragend) options.ondragend(event, element)
                    }
                }
                window.ontouchend = function (event) {
                    dragging_element = null
                    if (dragging_element_clone) {
                        dragging_element_clone.remove()
                        dragging_element_clone = null
                    }
                    if (dropzone_element) {
                        if (options.ondrop) options.ondrop(event, dropzone_element)
                    } else {
                        if (options.ondragend) options.ondragend(event, element)
                    }
                    setTimeout(function () {
                        document.documentElement.classList.remove("html-fixed")
                    }, 1000)
                }
                window.onmousemove = function (event) {
                    if (dragging_element) {
                        var width = dragging_element.getBoundingClientRect().width
                        var height = dragging_element.getBoundingClientRect().height
                        var mouseX = event.pageX
                        var mouseY = event.pageY
                        dragging_element_clone.style.top = (mouseY - height / 2) + "px"
                        dragging_element_clone.style.left = (mouseX - width / 2) + "px"
                    }
                    if (options.onmove) options.onmove(event, dragging_element)
                    document.querySelectorAll(dropzone_items).forEach(function (element) {
                        if (dragging_element && collapse(dragging_element_clone, element)) {
                            var drops = document.querySelectorAll(".can-drop")
                            if (drops.length == 0) {
                                element.classList.add("can-drop")
                                dropzone_element = element
                            }
                        } else {
                            if (element.classList.contains("can-drop")) {
                                element.classList.remove("can-drop")
                                dropzone_element = null
                            }
                        }
                    })
                }
                window.ontouchmove = function (event) {
                    if (!dragging_element) return
                    var width = dragging_element_clone.width
                    var height = dragging_element_clone.height
                    var mouseX = event.pageX
                    var mouseY = event.pageY
                    dragging_element_clone.style.top = (event.pageY - height / 2) + "px"
                    dragging_element_clone.style.left = (event.pageX - width / 2) + "px"
                    if (options.onmove) options.onmove(event, dragging_element)
                    document.querySelectorAll(dropzone_items).forEach(function (element) {
                        if (collapse(dragging_element_clone, element)) {
                            element.classList.add("can-drop")
                            dropzone_element = element
                        } else {
                            element.classList.remove("can-drop")
                            if (dropzone_element && dropzone_element.getAttribute("id") == element.getAttribute("id")) {
                                dropzone_element = null
                            }
                        }
                    })
                }
            })
        }
    }
    map = class {
        mapbox
        constructor(params) {
            this.checkMapBoxJS(() => {
                this.initMap(params)
            })
        }
        checkMapBoxJS(callback) {
            if (typeof mapboxgl === 'undefined') {
                var script = document.createElement('script')
                script.src = "https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.js"
                script.type = 'text/javascript'
                script.onload = () => {
                    this.checkMapBoxCSS(callback)
                }
                document.getElementsByTagName('head')[0].appendChild(script)
            } else {
                this.checkMapBoxCSS(callback)
            }
        }
        checkMapBoxCSS(callback) {
            var head = document.getElementsByTagName('head')[0]
            var link = document.createElement('link')
            link.rel = 'stylesheet'
            link.type = 'text/css'
            link.href = 'https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.css'
            link.media = 'all'
            head.appendChild(link)
            link.onload = () => {
                callback()
            }
        }
        initMap(params) {
            mapboxgl.accessToken = 'pk.eyJ1IjoicGFibG9tc2oiLCJhIjoiY2twcDN5aGI2MDNtNzJ3azk1YjVpbnN0dyJ9.3vk-ipLON5tKJokExHbxSw'
            this.mapbox = new mapboxgl.Map({
                container: params.container,
                style: params.style,
                zoom: params.zoom,
                center: params.center
            })
            this.mapbox.addControl(new mapboxgl.NavigationControl())
            this.mapbox.on('load', () => {
                this.initSources(params.sources)
                this.init3D()
            })
            if (params.onload) params.onload(this)
        }
        init3D() {
            var map = this.mapbox
            var layers = map.getStyle().layers
            var labelLayerId
            for (var i = 0; i < layers.length; i++) {
                if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
                    labelLayerId = layers[i].id
                    break
                }
            }
    
            // The 'building' layer in the Mapbox Streets
            // vector tileset contains building height data
            // from OpenStreetMap.
            map.addLayer({
                'id': 'add-3d-buildings',
                'source': 'composite',
                'source-layer': 'building',
                'filter': ['==', 'extrude', 'true'],
                'type': 'fill-extrusion',
                'minzoom': 15,
                'paint': {
                    'fill-extrusion-color': '#aaa',
    
                    // Use an 'interpolate' expression to
                    // add a smooth transition effect to
                    // the buildings as the user zooms in.
                    'fill-extrusion-height': [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        15,
                        0,
                        15.05,
                        ['get', 'height']
                    ],
                    'fill-extrusion-base': [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        15,
                        0,
                        15.05,
                        ['get', 'min_height']
                    ],
                    'fill-extrusion-opacity': 0.6
                }
            },
    
                labelLayerId
            )
        }
        initSources(sources) {
            for (var i = 0; i < sources.length; i++) this.initSource(sources[i])
        }
        initSource(source) {
            this.mapbox.addSource(source.name, source.options)
            var layer = source.layer
            layer.id = source.name
            layer.source = source.name
            this.mapbox.addLayer(layer)
            if (source.options.cluster) {
                this.mapbox.addLayer({
                    id: `${source.name}-cluster-count`,
                    type: 'symbol',
                    source: source.name,
                    filter: ['has', 'point_count'],
                    layout: {
                        'text-field': '{point_count_abbreviated}',
                        'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                        'text-size': 12
                    }
                })
                this.mapbox.addLayer({
                    id: `${source.name}-unclustered-point`,
                    type: 'circle',
                    source: source.name,
                    filter: ['!', ['has', 'point_count']],
                    paint: {
                        'circle-color': '#11b4da',
                        'circle-radius': 10,
                        'circle-stroke-width': 1,
                        'circle-stroke-color': '#fff'
                    }
                })
                this.mapbox.on('click', `${source.name}-unclustered-point`, (e) => {
                    if (source.onclick) source.onclick(e, this)
                })
                this.mapbox.on('mouseenter', `${source.name}-unclustered-point`, () => {
                    this.mapbox.getCanvas().style.cursor = 'pointer'
                })
                this.mapbox.on('mouseleave', `${source.name}-unclustered-point`, () => {
                    this.mapbox.getCanvas().style.cursor = ''
                })
            }
            if (source.onclick || source.options.cluster) {
                this.mapbox.on('click', source.name, (e) => {
                    if (source.options.cluster) {
                        var features = this.mapbox.queryRenderedFeatures(e.point, {
                            layers: [source.name]
                        })
                        var clusterId = features[0].properties.cluster_id
                        this.mapbox.getSource(source.name).getClusterExpansionZoom(
                            clusterId,
                            (err, zoom) => {
                                if (err) return
                                this.mapbox.easeTo({
                                    center: features[0].geometry.coordinates,
                                    zoom: zoom
                                })
                            }
                        )
                    }
                    if (source.onclick && !source.options.cluster) source.onclick(e, this)
                })
                this.mapbox.on('mouseenter', source.name, () => {
                    this.mapbox.getCanvas().style.cursor = 'pointer'
                })
                this.mapbox.on('mouseleave', source.name, () => {
                    this.mapbox.getCanvas().style.cursor = ''
                })
            }
        }
        showPopup(event, html) {
            new mapboxgl.Popup()
                .setLngLat(event.lngLat)
                .setHTML(html)
                .addTo(this.mapbox)
        }
        center(lat, lon, zoom) {
            this.mapbox.easeTo({
                center: {
                    lng: lon,
                    lat: lat
                },
                zoom: zoom
            })
        }
    }
    pdf = class {
        constructor(params) {
            this.checkLibraries(async () => {
                var A4_width = 620
                var A4_height = 877
                var orientation = (params && params.orientation) ? params.orientation : "p"
                if (orientation == "p") {
                    this.width = (params && params.width) ? params.width : A4_width
                    this.height = (params && params.height) ? params.height : A4_height
                } else {
                    this.width = (params && params.width) ? params.width : A4_height
                    this.height = (params && params.height) ? params.height : A4_width
                }
                this.margin_x = (params && params.margin_x) ? params.margin_x : 0
                this.margin_y = (params && params.margin_y) ? params.margin_y : 0
                this.jsPDF = new jsPDF({
                    orientation: orientation,
                    unit: 'px',
                    format: [this.width, this.height]
                })
                this.offset = this.margin_y
                if (params.background_color) this.background_color = params.background_color
                if (this.background_color) this.addbackground_color()
                if (params.id) await this.analyze(params.id)
                if (params.onFinish && typeof params.onFinish === 'function') setTimeout(params.onFinish, 100)
            })
        }
        checkLibraries(callback) {
            this.checkJsPdf(callback)
        }
        checkJsPdf(callback) {
            if (typeof jspdf === 'undefined') {
                var script = document.createElement('script')
                script.src = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"
                script.type = 'text/javascript'
                script.onload = () => {
                    this.checkHtml2Canvas(callback)
                }
                document.getElementsByTagName('head')[0].appendChild(script)
            } else {
                this.checkHtml2Canvas(callback)
            }
        }
        checkHtml2Canvas(callback) {
            if (typeof jspdf === 'undefined') {
                var script = document.createElement('script')
                script.src = "https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"
                script.type = 'text/javascript'
                script.onload = () => {
                    this.checkDomToImage(callback)
                }
                document.getElementsByTagName('head')[0].appendChild(script)
            } else {
                this.checkDomToImage(callback)
            }
        }
        checkDomToImage(callback) {
            if (typeof domtoimage === 'undefined') {
                var script = document.createElement('script')
                script.src = "https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.min.js"
                script.type = 'text/javascript'
                script.onload = () => {
                    callback()
                }
                document.getElementsByTagName('head')[0].appendChild(script)
            } else {
                callback()
            }
        }
        getProp() {
            var prop = 1024 / (this.width - this.margin_x * 2)
            if (prop < 1) return 1
            return prop
        }
        async analyze(id) {
            var prop = this.getProp()
            var width = this.width * prop
            var height = this.height * prop
            var margin_x = this.margin_x
            var margin_y = this.margin_y
            var pdf = document.getElementById(id)
            pdf.style.zIndex = -1
            pdf.style.position = "fixed"
            pdf.style.top = "0px"
            pdf.style.left = `0px`
            pdf.style.width = width + "px"
            pdf.style.height = height + "px"
            pdf.style.overflow = "auto"
            var pages = pdf.querySelectorAll(".dpdf-page")
            for (var i = 0; i < pages.length; i++) {
                pages[i].style.display = "none"
            }
            for (var i = 0; i < pages.length; i++) {
                if (i > 0) this.addPage()
                var page = pages[i]
                page.style.display = "block"
                page.style.background_color = "white"
                page.style.width = (width - (margin_x * 2)) + "px"
                page.style.height = (height - (margin_y * 2)) + "px"
                await this.addElement(page)
                page.style.display = "none"
            }
            pdf.attributeStyleMap.clear()
            for (var i = 0; i < pages.length; i++) {
                pages[i].attributeStyleMap.clear()
            }
        }
        addPage() {
            if (!this.jsPDF) return
            this.jsPDF.addPage(this.width, this.height)
            this.offset = this.margin_y
            if (this.background_color) this.addbackground_color()
        }
        addbackground_color() {
            var canvas = document.createElement("canvas")
            canvas.width = 100
            canvas.height = 100
            var ctx = canvas.getContext("2d")
            ctx.fillStyle = this.background_color
            ctx.fillRect(0, 0, canvas.width, canvas.height)
            var url = canvas.toDataURL("image/png", 1.0)
            this.jsPDF.addImage(url, 'JPG', 0, 0, this.width, this.height)
        }
        async add(query, full = false) {
            var elements = document.querySelectorAll(query)
            if (elements.length == 0) return
            var element = elements[0]
            await this.addElement(element, full)
        }
        async addElement(element, full = true) {
            var canvas = await this.elementToCanvas(element)
            this.addCanvas(canvas, full)
        }
        async elementToCanvas(element) {
            return new Promise((resolve, reject) => {
                html2canvas(element, {
                    background: '#ffffff',
                    onrendered: function (canvas) {
                        resolve(canvas)
                    }
                })
            })
        }
        async elementToImage(element) {
            return new Promise((resolve, reject) => {
                var options = {
                    quality: 1
                }
                domtoimage.toJpeg(element, options).then(function (dataUrl) {
                    resolve(dataUrl)
                })
            })
        }
        addImage(url, full = true) {
            var max_width = this.width * 0.72
            var max_height = this.height * 0.71
            var width = this.width * 0.72
            var height = this.height * 0.71
            if (full) {
                this.jsPDF.addImage(url, 'PNG', this.margin_x, this.margin_y, width, height)
                return
            }
            // var prop = width / canvas.width
            // width = canvas.width * prop
            // height = canvas.height * prop
            if (height > max_height - this.margin_y) height = max_height - this.margin_y
            if (max_height - this.offset < height) this.addPage()
            this.jsPDF.addImage(url, 'JPG', this.margin_x, this.offset + this.margin_y, width, height)
            this.offset += height
            this.offset += (this.margin_y * 2)
        }
        addCanvas(canvas, full = true) {
            var max_width = this.width * 0.72
            var max_height = this.height * 0.71
            var width = this.width * 0.72
            var height = this.height * 0.71
            var url = canvas.toDataURL("image/png", 1.0)
            if (full) {
                this.jsPDF.addImage(url, 'PNG', this.margin_x, this.margin_y, width, height)
                return
            }
            var prop = width / canvas.width
            width = canvas.width * prop
            height = canvas.height * prop
            if (height > max_height - this.margin_y) height = max_height - this.margin_y
            if (max_height - this.offset < height) this.addPage()
            this.jsPDF.addImage(url, 'JPG', this.margin_x, this.offset + this.margin_y, width, height)
            this.offset += height
            this.offset += (this.margin_y * 2)
        }
        export(name = "test") {
            this.jsPDF.save(`${name}.pdf`)
        }
    }
    table = class {
        constructor(params) {
            if (!params) return
            if (params.data && Array.isArray(params.data)) this.parse(params.data)
            if (params.onFinish && typeof params.onFinish === 'function') setTimeout(params.onFinish, 100)
        }
        parse(data) {
            if (!data || !Array.isArray(data) || data.length == 0) return
            this.table = {
                thead: this.getHead(data[0]),
                tbody: this.getBody(data)
            }
        }
        getBody(data) {
            var body = []
            var arr = this.cleanArr(data)
            for (var i = 0; i < arr.length; i++) {
                var row = []
                for (var j = 0; j < arr[i].length; j++) {
                    row.push({
                        text: arr[i][j],
                        colspan: 1
                    })
                }
                body.push(row)
            }
            return body
        }
        cleanArr(data) {
            var arr = []
            for (var i = 0; i < data.length; i++) {
                arr.push(this.cleanItem(data[i]))
            }
            return arr
        }
        cleanItem(item, aux = []) {
            var keys = Object.keys(item)
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i]
                if (typeof item[key] === 'object' && item[key] !== null) {
                    aux = this.cleanItem(item[key], aux)
                } else {
                    aux.push(item[key])
                }
            }
            return aux
        }
        getHead(item) {
            var thead = this.getHeadRows(item, [])
            var maxCols = this.getMaxCols(item)
            var staticCols = this.getStaticCols(item)
            for (var i = 0; i < thead.length; i++) {
                var ok = true
                var ok2 = true
                var count = 0
                var row = thead[i]
                for (var j = 0; j < row.length; j++) {
                    var col = row[j]
                    var colspan = col.colspan
                    count += colspan
                }
                if (count < (maxCols - staticCols)) ok = false
                if (count < maxCols) ok2 = false
                if (ok == false) {
                    var diff = (maxCols - staticCols) / count
                    var len = row.length
                    for (var j = 0; j < diff - 1; j++) {
                        for (let k = 0; k < len; k++) {
                            row.push(row[k])
                            count += row[k].colspan
                        }
                    }
                }
                if (ok2 == false) {
                    var diff2 = maxCols - count
                    for (var j = 0; j < diff2; j++) {
                        row.push({
                            text: "",
                            colspan: 1
                        })
                    }
                }
            }
            return thead
        }
        getHeadRows(item, trs) {
            var cols = this.getHeadCols(item)
            trs.push(cols)
            for (var i = 0; i < cols.length; i++) {
                var col = cols[i]
                var text = col.text
                if (typeof item[text] === 'object' && item[text] !== null) {
                    return this.getHeadRows(item[text], trs)
                }
            }
            return trs
        }
        getHeadCols(item) {
            var keys = Object.keys(item)
            var tr = []
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i]
                var colspan = 1
                if (typeof item[key] === 'object' && item[key] !== null) {
                    colspan = this.getMaxCols(item[key])
                }
                tr.push({
                    text: key,
                    colspan: colspan
                })
            }
            return tr
        }
        getSubLevels(item) {
            var keys = Object.keys(item)
            var count = 1
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i]
                if (typeof item[key] === 'object' && item[key] !== null) {
                    count += this.getSubLevels(item[key])
                    break
                }
            }
            return count
        }
        getStaticCols(item) {
            var keys = Object.keys(item)
            var count = keys.length
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i]
                if (typeof item[key] === 'object' && item[key] !== null) {
                    count -= 1
                }
            }
            return count
        }
        getMaxCols(item) {
            var keys = Object.keys(item)
            var count = keys.length
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i]
                if (typeof item[key] === 'object' && item[key] !== null) {
                    count -= 1
                    count += this.getMaxCols(item[key])
                }
            }
            return count
        }
        draw(params) {
            if (!params) return
            if (!params.id) return
            if (this.table == null) return
            var table = document.getElementById(params.id)
            if (!table) return
            if (params.classes && Array.isArray(params.classes) && params.classes.length > 0) {
                for (var i = 0; i < params.classes.length; i++) {
                    table.classList.add(params.classes[i])
                }
            }
            var thead = document.createElement("thead")
            var tbody = document.createElement("tbody")
            for (var i = 0; i < this.table.thead.length; i++) {
                var row = this.table.thead[i]
                var tr = document.createElement("tr")
                // inverse
                for (var j = row.length - 1; j >= 0; j--) {
                    var col = row[j]
                    var th = document.createElement("th")
                    th.innerHTML = col.text
                    th.colSpan = col.colspan
                    tr.appendChild(th)
                }
                thead.appendChild(tr)
            }
            table.appendChild(thead)
            for (var i = 0; i < this.table.tbody.length; i++) {
                var row = this.table.tbody[i]
                var tr = document.createElement("tr")
                // inverse
                for (var j = row.length - 1; j >= 0; j--) {
                    var col = row[j]
                    var td = document.createElement("td")
                    td.innerHTML = col.text
                    th.colSpan = col.colspan
                    tr.appendChild(td)
                }
                tbody.appendChild(tr)
            }
            table.appendChild(tbody)
            if (params.onFinish && typeof params.onFinish === 'function') setTimeout(params.onFinish, 100)
        }
        toJSON() {
            let json = []
            if (!this.table) return json
            if (!this.table.thead) return json
            if (!this.table.tbody) return json
            let columns = []
            let numColumns = this.table.thead[this.table.thead.length - 1].length;
            for (let i = 0; i < numColumns; i++) columns.push("")
            for (let i = 0; i < this.table.thead.length; i++) {
                let row = this.table.thead[i]
                let col = numColumns - 1
                for (let j = 0; j < row.length; j++) {
                    let column = row[j].text
                    if (row[j].colspan > 1) {
                        col -= row[j].colspan
                        continue
                    }
                    if (columns[col] != "") {
                        col--
                        continue
                    }
                    columns[col] = column
                    col--
                }
            }
            for (let row of this.table.tbody) {
                let newRow = {}
                for (let i = 0; i < columns.length; i++) {
                    let column = columns[i]
                    newRow[column] = row[columns.length - 1 - i].text
                }
                json.push(newRow)
            }
            return json
        }
        export(params) {
            if (!params) return
            if (!params.name) return
            if (!params.format) return
            if (typeof alasql === 'undefined') {
                var script = document.createElement('script')
                script.src = "https://cdnjs.cloudflare.com/ajax/libs/alasql/0.6.2/alasql.min.js"
                script.type = 'text/javascript'
                script.onload = () => {
                    this.alasqlExport(params.name, params.format)
                }
                document.getElementsByTagName('head')[0].appendChild(script)
            } else {
                this.alasqlExport(params.name, params.format)
            }
        }
        alasqlExport(name, format) {
            alasql(`SELECT * INTO ${format}('${name}.${format}',{headers:true}) FROM ?`, [this.toJSON()])
        }
    }
    app = class {
        constructor() {
            document.body.setAttribute("id", "devetty-app")
            this.states = {}
            this.current_state = null
        }
        load(options) {
            if (options.states) {
                for(var state in options.states) {
                    this.addState(state, options.states[state])
                }
            }
        }
        component = class {
            constructor(options) {
                this.tag = options.tag || "div"
                this.attributes = options.attributes || {}
                this.classes = options.classes || []
                this.html = options.html || ""
                this.children = options.children || []
                this.onClick = options.onClick || null
            }
            draw(container) {
                var element = document.createElement(this.tag)
                for(var attribute in this.attributes) {
                    element.setAttribute(attribute, this.attributes[attribute])
                }
                element.classList = this.classes.join(" ")
                element.innerHTML = this.html
                element.onclick = function(event) {
                    if (this.onClick) this.onClick(event)
                }.bind(this)
                container.appendChild(element)
                for(var child of this.children) child.draw(element)
            }
        }
        parseHTML(query) {
            var elements = document.querySelectorAll(query)
            var components = []
            for (var i = 0; i < elements.length; i++) {
                components.push(this.parseElement(elements[i]))
            }
            if (components.length == 1) return components[0]
            return components
        }
        isAlphanumeric(string) {
            return string.match(/^[^a-zA-Z0-9]+$/) ? true : false;
        }
        parseElement(element) {
            var children = []
            for (var i = 0; i < element.children.length; i++) {
                children.push(this.parseElement(element.children[i]))
            }
            var classes = []
            for(var i = 0; i < element.classList.length; i++) {
                classes.push(element.classList[i])
            }
            var component = {
                tag: element.tagName,
                children: children,
                classes: classes,
                html:  element.hasAttribute("html") ? element.getAttribute("html") : ""
            }
            return component
        }
        setCurrentState(state) {
            document.body.classList.remove(this.current_state)
            this.current_state = state
            document.body.classList = [this.current_state]
            this.draw()
        }
        addState(id, components) {
            for(var i = 0; i < components.length; i++) {
                if (components[i] instanceof this.component) continue
                components[i] = this.parseComponent(components[i])
            }
            this.states[id] = components
            if (!this.current_state) this.setCurrentState(id)
        }
        parseComponent(component) {
            if (!component.children) component.children = []
            for(var i = 0; i < component.children.length; i++) {
                if (component.children[i] instanceof this.component) continue
                component.children[i] = this.parseComponent(component.children[i])
            }
            component = new this.component(component)
            return component
        }
        draw() {
            document.body.innerHTML = ""
            if (this.current_state == null) return
            for (var component of this.states[this.current_state]) component.draw(document.body)
        }
    }
})())
